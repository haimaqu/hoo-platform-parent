package com.hoo;


import com.hoo.utils.SystemInfoUtils;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    public static void main(String[] args) throws Exception {
        System.out.println("cpu信息 : " + SystemInfoUtils.getCpuInfo().toString());
        System.out.println("jvm信息 : " + SystemInfoUtils.getJvmInfo().toString());

        System.out.println("内存信息 : " + SystemInfoUtils.getMemInfo().toString());

        System.out.println("系统文件 : " + SystemInfoUtils.getSysFileInfo().toString());

        System.out.println("系统信息 : " + SystemInfoUtils.getSysInfo().toString());
    }
}
