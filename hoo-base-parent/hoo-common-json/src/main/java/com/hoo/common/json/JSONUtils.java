package com.hoo.common.json;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * JSON 简易工具类封装（待扩展）
 *
 * 缘由: 避免类似fastjson安全漏洞,引发关联项目安全问题，这里直接升级或同版本重新构建即可
 * @author hank
 * @create 2020-06-28 下午5:51
 **/

public class JSONUtils {

    static ObjectMapper objectMapper;
    /**
     * 解析json为范型对象
     *
     * @param content
     * @param valueType
     * @return
     */
    public static <T> T parse(String content, Class<T> valueType) {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
        }
        try {
            return objectMapper.readValue(content, valueType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 生成json字符串
     *
     * @param object
     * @return
     */
    public static String toJSONString(Object object) {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
        }
        try {
            return objectMapper.writeValueAsString(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
