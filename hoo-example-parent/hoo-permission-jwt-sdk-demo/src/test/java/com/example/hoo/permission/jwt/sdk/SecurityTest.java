package com.example.hoo.permission.jwt.sdk;

import com.hoo.permission.sdk.web.config.SafeConfig;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 安全测试
 *
 * @author hank
 * @create 2020-11-12 上午11:26
 **/
@SpringBootTest
public class SecurityTest {


    @Autowired SafeConfig safeConfig;


    @Test
    public void testMD5Password() {
        String hashAlgorithName = safeConfig.getHashAlgorithmName();
        // String username = "han.q"; 3bcd1b3e7b21fd3feea34b237402be98; han.qing b3a9dc4f4c5d86aeb1fd74d0ba89f292;
        String password = "123456";
        String salt = "b3a9dc4f4c5d86aeb1fd74d0ba89f292";
        int hashIterations = safeConfig.getHashIterations();

        ByteSource credentialsSalt = ByteSource.Util.bytes(salt);
        SimpleHash simpleHash = new SimpleHash(hashAlgorithName, password, credentialsSalt, hashIterations);
        System.out.println(simpleHash);
    }

}
