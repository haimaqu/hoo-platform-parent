/*
 Navicat Premium Data Transfer

 Source Server         : permission_platform
 Source Server Type    : MySQL
 Source Server Version : 50639
 Source Host           : 127.0.0.1:3306
 Source Schema         : permission_platform2

 Target Server Type    : MySQL
 Target Server Version : 50639
 File Encoding         : 65001

 Date: 24/12/2020 00:07:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_sys_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_dict_item`;
CREATE TABLE `t_sys_dict_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增组件',
  `dict_code` varchar(255) NOT NULL COMMENT '所属字典类型编码',
  `label` varchar(50) NOT NULL COMMENT '字典项-显示值',
  `value` varchar(50) NOT NULL COMMENT '字典项-实际值',
  `sort_num` int(3) DEFAULT NULL COMMENT '排序号',
  `optional_scenario` varchar(255) DEFAULT NULL COMMENT '可选场景, 供前端灵活处理（逗号分隔, 如：该项可在查询是用，但是新增时禁选（不可见），修改可展示，但禁用）',
  `use_flag` varchar(255) NOT NULL DEFAULT '1' COMMENT '可用状态 1 可用， 0 不可用',
  `description` varchar(255) DEFAULT NULL COMMENT '用途描述说明',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建者',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(255) DEFAULT NULL COMMENT '修改者',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='系统表 - 字典项';

-- ----------------------------
-- Records of t_sys_dict_item
-- ----------------------------
BEGIN;
INSERT INTO `t_sys_dict_item` VALUES (1, 'sys_dict_type', '系统级', 'system', 1, 'add、update、query', '1', '', '2020-12-21 14:42:51', '', '2020-12-23 19:00:20', '');
INSERT INTO `t_sys_dict_item` VALUES (2, 'sys_dict_type', '业务级', 'business', 2, 'add、update、query', '1', '', '2020-12-21 14:42:55', '', '2020-12-23 18:54:33', '');
INSERT INTO `t_sys_dict_item` VALUES (3, 'sys_del_flag', '已删除', '1', 0, 'add、update、query', '1', '', '2020-12-23 17:32:52', '', '2020-12-23 17:35:22', '');
INSERT INTO `t_sys_dict_item` VALUES (4, 'sys_del_flag', '未删除', '0', 1, 'add、update、query', '1', NULL, '2020-12-23 17:32:55', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_item` VALUES (9, 'sys_use_flag', '可用', '1', 0, 'add、update、query', '1', NULL, '2020-12-23 20:42:51', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_item` VALUES (10, 'sys_use_flag', '不可用', '0', 1, 'add、update、query', '1', NULL, '2020-12-23 20:43:08', NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for t_sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_dict_type`;
CREATE TABLE `t_sys_dict_type` (
  `code` varchar(50) NOT NULL COMMENT '字典类型编码【不可修改】',
  `name` varchar(255) NOT NULL COMMENT '字典显示名称',
  `description` varchar(255) DEFAULT NULL COMMENT '用途描述说明',
  `use_flag` varchar(1) DEFAULT '1' COMMENT '使用状态 1 可用 0 不可用',
  `item_data_type` varchar(8) DEFAULT NULL COMMENT '元素数据类型: string 字符 , number 数字',
  `dict_type` varchar(10) DEFAULT NULL COMMENT '数据字典类型: system 系统级，business 业务级',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建者',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(255) DEFAULT NULL COMMENT '修改者',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统表 - 字典类型表';

-- ----------------------------
-- Records of t_sys_dict_type
-- ----------------------------
BEGIN;
INSERT INTO `t_sys_dict_type` VALUES ('sys_del_flag', '删除标记', '1 删除，0 未删除', '1', NULL, 'system', '2020-12-21 14:29:29', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_type` VALUES ('sys_dict_optional_scenario', '字典表-可选场景', 'add,update,query 对应新增、修改、查询场景', '1', NULL, 'system', '2020-12-21 17:25:10', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_type` VALUES ('sys_dict_type', '字典分类（类型）', 'system 系统级，business 业务级', '1', NULL, 'system', '2020-12-21 14:34:37', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_type` VALUES ('sys_resource_status', '资源状态', '-1 全部，1正常，0 禁用', '1', NULL, 'system', '2020-12-21 17:29:26', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_type` VALUES ('sys_role_status', '角色状态', '-1 全部，1正常，0 禁用', '1', '', 'system', '2020-12-21 17:29:29', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_type` VALUES ('sys_user_status', '系统 - 用户状态', '0 禁用，1 正常，2 锁定', '1', NULL, 'system', '2020-12-21 17:25:07', NULL, NULL, NULL);
INSERT INTO `t_sys_dict_type` VALUES ('sys_use_flag', '可用标记', '1 可用 ，0 不可用', '1', NULL, 'system', '2020-12-21 14:32:15', NULL, NULL, NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
