package com.hoo.permission.sdk.server.dao;

import com.hoo.common.model.Page;
import com.hoo.permission.sdk.server.domain.entity.SysResource;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 系统-资源表(菜单、按钮) Dao层接口
 * @author 小韩工作室
 * @date 2020-06-28 16:51:32
 */
public interface ISysResourceDao {
    /**
     * 新增 - 系统-资源表(菜单、按钮)
     */
    boolean add(SysResource entity);

    /**
     * 修改 - 系统-资源表(菜单、按钮)
     */
    boolean update(SysResource entity);

    /**
     * 删除 - 系统-资源表(菜单、按钮)
     */
    boolean delete(SysResource entity);

    /**
     * 删除 - 系统-资源表(菜单、按钮)
     */
    boolean delete(Serializable id);

    /**
     * 批量通过IDs删除
     * @param ids
     * @return
     */
    boolean batchDelete(List<Long> ids);
    /**
     * 根据父节点删除对应子节点
     * @param parentId
     * @return
     */
    boolean deleteByParentId(Long parentId);

    /**
     * 根据主键 获取记录
     */
    SysResource get(Serializable id);

    /**
     * 根据条件查询所有记录
     */
    List<SysResource> findAll(Map<String, Object> params);

    /**
     * 分页查询
     * @param page
     * @param params
     * @return
     */
    Page<SysResource> query(Page page, Map<String, Object> params);
}
