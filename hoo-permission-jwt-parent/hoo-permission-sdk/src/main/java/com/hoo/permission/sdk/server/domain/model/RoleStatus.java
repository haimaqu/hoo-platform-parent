package com.hoo.permission.sdk.server.domain.model;

/**
 * 角色状态
 * @author hank
 */
public interface RoleStatus {
    /**
     * 正常
     */
    String NORMAL = "1";
    /**
     * 禁用
     */
    String DISABLED = "0";
}
