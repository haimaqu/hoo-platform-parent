package com.hoo.permission.sdk.server.dao;

import com.hoo.permission.sdk.server.domain.entity.SysRole;
import com.hoo.permission.sdk.server.domain.entity.SysRoleUser;

import java.io.Serializable;
import java.util.List;

/**
 * 系统-角色用户关系表 Dao层接口
 * @author 小韩工作室
 * @date 2020-06-28 18:15:48
 */
public interface ISysRoleUserDao {
    /**
     * 新增 - 系统-角色用户关系表
     */
    boolean add(Long userId, List<Long> roleIds);

    /**
     * 删除 - 系统-角色用户关系表
     */
    // boolean delete(SysRoleUser entity);

    /**
     * 通过 角色 删除关系
     * @param roleId
     * @return
     */
    boolean deleteByRoleId(Serializable roleId);

    /**
     * 通过 用户 删除关系
     * @param userId
     * @return
     */
    boolean deleteByUserId(Serializable userId);

    /**
     * 根据用户获取其角色
     * @param userId
     * @return
     */
    List<SysRole> getRoles(Serializable userId);

    /**
     * 根据用户获取其角色IDs
     * @param userId
     * @return
     */
    List<Long> getRoleIds(Serializable userId);
}
