package com.hoo.permission.sdk.server.domain.entity;

import java.io.Serializable;

/**
 * 系统-角色表
 * @author 小韩工作室
 * @date 2020-06-28 16:51:32
 */
public class SysRole implements Serializable {
    /**
     * ID主键
     */
    private Long id;
    /**
     * 唯一识别标记（不可修改）
     */
    private String roleKey;
    /**
     * 角色名称（标记）
     */
    private String name;
    /**
     * 角色描述
     */
    private String description;
    /**
     * 角色-状态
     * @see {com.hoo.permission.sdk.server.domain.model.RoleStatus}
     */
    private String status;

    public SysRole(){
        super();
    }

    public Long getId(){
        return this.id;
    }

    public String getRoleKey() {
        return roleKey;
    }

    public void setRoleKey(String roleKey) {
        this.roleKey = roleKey;
    }

    public String getName(){
        return this.name;
    }

    public String getDescription(){
        return this.description;
    }

    public void setId(Long id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
