package com.hoo.permission.sdk.server.dao;

import com.hoo.permission.sdk.server.domain.entity.SysRoleResource;

import java.util.List;

/**
 * 系统-角色资源关系表 Dao层接口
 * @author 小韩工作室
 * @date 2020-06-28 18:15:48
 */
public interface ISysRoleResourceDao {
    /**
     * 新增 - 系统-角色资源关系表
     */
    // boolean add(SysRoleResource entity);

    /**
     * 删除 - 系统-角色资源关系表
     */
    // boolean delete(SysRoleResource entity);


    /**
     * 新增/修改 - 系统-角色资源关系表
     */
    boolean add(Long roleId, List<Long> resourceIds);

    /**
     * 通过roleId删除 - 系统-角色资源关系
     * @param roleId
     * @return
     */
    boolean deleteByRoleId(Long roleId);

    /**
     * 根据roleId 查询对应关联资源
     * @param roleId
     * @return
     */
    List<Long> queryResourceIds(Long roleId);

    /**
     * 通过 resourceId 删除 - 系统-角色资源关系
     * @param resourceId
     * @return
     */
    boolean deleteByResourceId(Long resourceId);

    /**
     * 通过 resourceIds 批量删除 - 系统-角色资源关系
     * @param resourceIds
     * @return
     */
    boolean batchDeleteByResourceIds(List<Long> resourceIds);
}
