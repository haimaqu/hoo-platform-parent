package com.hoo.permission.sdk.server.service;

import com.hoo.permission.sdk.server.domain.pojo.SysRolePo;
import com.hoo.permission.sdk.server.domain.entity.SysRole;

import java.util.List;
import java.util.Map;

/**
 * 角色服务层
 *
 * @author hank
 * @create 2020-06-28 下午7:45
 **/

public interface ISysRoleService {

    /**
     * 保存角色
     * 1、保存角色信息
     * 2、权限资源信息（先删除，再添加）
     */
    void save(SysRolePo roleDto);

    /**
     * 删除角色
     * 删除关联信息（角色用户、角色资源）、角色自身信息
     * @param roleIds
     */
    void delete(List<Long> roleIds);

    boolean updateStatus(Long id, String status);

    /**
     * 根据条件 查询所有
     * @param params
     * @return
     */
    List<SysRole> queryAll(Map<String, Object> params);

}
