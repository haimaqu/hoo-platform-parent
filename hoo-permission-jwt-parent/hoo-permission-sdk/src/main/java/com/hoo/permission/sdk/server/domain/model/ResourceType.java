package com.hoo.permission.sdk.server.domain.model;

/**
 * 资源类型
 *
 * @author hank
 * @create 2020-09-18 下午8:59
 **/

public interface ResourceType {
    /**
     * 菜单
     */
    int URL = 1;
    /**
     * 按钮
     */
    int BUTTON = 2;
}
