package com.hoo.permission.sdk.server.service;

import com.hoo.common.model.Page;
import com.hoo.permission.sdk.server.domain.entity.SysApp;
import com.hoo.permission.sdk.server.domain.pojo.SysAppPo;

import java.util.List;
import java.util.Map;

/**
 * 系统应用服务
 * @author hank
 */
public interface ISysAppService {

    /**
     * 新增
     * @param entity
     * @return
     */
    boolean save(SysApp entity);

    /**
     * 删除
     * @param ids
     * @return
     */
    boolean delete(List<Long> ids);

    /**
     * 修改
     * @param entity
     * @return
     */
    boolean update(SysApp entity);

    /**
     * 分页查询
     * @param page
     * @param params
     * @return
     */
    Page query(Page page, SysAppPo params);

}
