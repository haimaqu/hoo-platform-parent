package com.hoo.permission.sdk.server.util;

import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 字符工具类
 *
 * @author hank
 * @create 2020-07-04 下午12:39
 **/

public class StringUtil {

    /**
     * 转换逗号分隔的long字符串为Long数字
     * @param strs
     * @return
     */
    public static List<Long> parseLong(String strs) {
        List<Long> ls = new ArrayList<>();
        if(!StringUtils.isEmpty(strs)) {
            for(String str : strs.split(",")) {
                try {
                    ls.add(Long.valueOf(str));
                }catch (NumberFormatException e){}
            }
        }
        return ls;
    }
}
