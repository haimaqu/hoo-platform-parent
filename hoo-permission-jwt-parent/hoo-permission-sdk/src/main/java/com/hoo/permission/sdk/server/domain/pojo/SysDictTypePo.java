package com.hoo.permission.sdk.server.domain.pojo;

import com.hoo.permission.sdk.server.domain.entity.SysDictType;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统表 - 字典类型表
 * @author 小韩工作室
 * @date 2020-12-21 09:37:51
 */
public class SysDictTypePo extends SysDictType {
    /**
     * 搜索关键词 = 类型编码 + 类型名称 + 描述 code, name, description
     */
    private String searchWord;

    public String getSearchWord() {
        return searchWord;
    }

    public void setSearchWord(String searchWord) {
        this.searchWord = searchWord;
    }
}
