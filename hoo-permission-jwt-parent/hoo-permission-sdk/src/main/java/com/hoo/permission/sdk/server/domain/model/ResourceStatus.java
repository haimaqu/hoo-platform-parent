package com.hoo.permission.sdk.server.domain.model;

public interface ResourceStatus {
    /**
     * 正常
     */
    String NORMAL = "1";
    /**
     * 禁用
     */
    String DISABLED = "0";
}
