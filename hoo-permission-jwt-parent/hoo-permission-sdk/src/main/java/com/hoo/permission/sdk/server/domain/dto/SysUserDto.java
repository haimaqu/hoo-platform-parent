package com.hoo.permission.sdk.server.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hoo.permission.sdk.server.domain.entity.SysUser;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 系统-用户DTO模型
 *
 * @author hank
 * @create 2020-10-20 下午6:53
 **/

public class SysUserDto extends SysUser {
    /**
     * 向外输出时，忽略 password、salt
     */
    @JsonIgnore
    private String password;
    @JsonIgnore
    private String salt;

    /**
     * 逗号分隔的 角色ids
     */
    @JsonIgnore
    private String roleIdStr;
    /**
     * 角色ID集合
     */
    private List<Long> roleIds;
    /**
     * 逗号分隔的资源ids
     */
    @JsonIgnore
    private String resourceIdStr;
    /**
     * 资源ID集合
     */
    private List<Long> resourceIds;

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getSalt() {
        return salt;
    }

    @Override
    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getRoleIdStr() {
        return roleIdStr;
    }

    public void setRoleIdStr(String roleIdStr) {
        this.roleIdStr = roleIdStr;
    }

    public List<Long> getRoleIds() {
        if(roleIds == null) {
            roleIds = new ArrayList<>();
            for (String id : (null != roleIdStr ? roleIdStr : "").split(",")) {
                if(StringUtils.isEmpty(id)) { continue; }
                roleIds.add(Long.valueOf(id));
            }
        }
        return roleIds;
    }

    public void setRoleIds(List<Long> roleIds) {
        this.roleIds = roleIds;
    }

    public String getResourceIdStr() {
        return resourceIdStr;
    }

    public void setResourceIdStr(String resourceIdStr) {
        this.resourceIdStr = resourceIdStr;
    }

    public List<Long> getResourceIds() {
        if(resourceIds == null) {
            resourceIds = new ArrayList<>();
            for (String id : (null != resourceIdStr ? resourceIdStr : "").split(",")) {
                if(StringUtils.isEmpty(id)) { continue; }
                resourceIds.add(Long.valueOf(id));
            }
        }
        return resourceIds;
    }

    public void setResourceIds(List<Long> resourceIds) {
        this.resourceIds = resourceIds;
    }
}
