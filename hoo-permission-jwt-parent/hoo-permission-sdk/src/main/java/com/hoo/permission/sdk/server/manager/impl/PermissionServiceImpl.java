package com.hoo.permission.sdk.server.manager.impl;

import com.alibaba.fastjson.JSONArray;
import com.hoo.permission.sdk.server.dao.ISysRoleUserDao;
import com.hoo.permission.sdk.server.dao.ISysUserDao;
import com.hoo.permission.sdk.server.domain.entity.SysResource;
import com.hoo.permission.sdk.server.domain.entity.SysRole;
import com.hoo.permission.sdk.server.domain.model.ResourceStatus;
import com.hoo.permission.sdk.server.manager.IPermissionService;
import com.hoo.permission.sdk.server.service.ISysResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.*;

/**
 * 权限服务实现类
 *
 * @author hank
 * @create 2020-07-04 下午7:08
 **/
@Service
public class PermissionServiceImpl implements IPermissionService {

    @Autowired ISysUserDao userDao;

    @Autowired ISysResourceService resourceService;

    @Autowired ISysRoleUserDao roleUserDao;

    @Override
    public Set<String> getPermissions(Long userId) {
        Set<String> perms = new HashSet<>();
        List<String> list = userDao.queryAllPerms(userId);
        for(String perm : list) {
            if(StringUtils.isEmpty(perm)) {
                continue;
            }
            perms.addAll(Arrays.asList(perm.trim().split(",")));
        }
        return perms;
    }

    @Override
    public List<SysRole> getRoles(Serializable userId) {
        return roleUserDao.getRoles(userId);
    }

    @Override
    public JSONArray getMenus(Long userId) {
        Map<String,Object> params = new HashMap<>();
        params.put("type", 1);
        params.put("status", ResourceStatus.NORMAL);
        params.put("userId", userId);
        return resourceService.queryTree(params);
    }

    @Override
    public List<SysResource> queryResource(Long userId) {
        Map<String,Object> params = new HashMap<>();
        params.put("type", 1);
        params.put("status", ResourceStatus.NORMAL);
        params.put("userId", userId);
        return resourceService.queryList(params);
    }
}
