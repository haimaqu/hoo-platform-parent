package com.hoo.permission.sdk.server.service;

import com.alibaba.fastjson.JSONArray;
import com.hoo.permission.sdk.server.domain.entity.SysResource;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 系统资源服务层
 * @author hank
 */
public interface ISysResourceService {

    /**
     * 新增系统资源
     * @param entity
     * @return
     * @throws Exception
     */
    boolean add(SysResource entity);

    /**
     * 修改系统资源
     * @param entity
     * @return
     * @throws Exception
     */
    boolean update(SysResource entity);

    /**
     * 删除资源信息
     * @param resourceId
     * @return
     */
    boolean delete(List<Long> resourceId);

    /**
     * 根据ID获取资源
     * @param id
     * @return
     */
    SysResource get(Serializable id);

    /**
     * 获取资源列表
     * @param params
     * @return
     */
    List<SysResource> queryList(Map<String,Object> params);

    /**
     * 获取资源树集合
     * @param params
     * @return
     */
    JSONArray queryTree(Map<String,Object> params);
}
