package com.hoo.permission.sdk.server.dao.impl;

import com.hoo.common.model.Page;
import com.hoo.permission.sdk.server.dao.ISysDictTypeDao;
import com.hoo.permission.sdk.server.domain.entity.SysDictType;
import com.hoo.permission.sdk.server.domain.pojo.SysDictTypePo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 系统表 - 字典类型表 Dao层 实现
 * @author 小韩工作室
 * @date 2020-12-21 09:37:51
 */
@Repository
public class SysDictTypeDaoImpl implements ISysDictTypeDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public boolean add(SysDictType entity){
        return jdbcTemplate.update("INSERT INTO t_sys_dict_type(code, name, description, use_flag, item_data_type, create_time, create_by, update_time, update_by, dict_type) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", entity.getCode(), entity.getName(), entity.getDescription(), entity.getUseFlag(), entity.getItemDataType(), entity.getCreateTime(), entity.getCreateBy(), entity.getUpdateTime(), entity.getUpdateBy(), entity.getDictType()) > 0;
    }

    @Override
    public boolean update(SysDictType entity) {
        return jdbcTemplate.update("UPDATE t_sys_dict_type SET name = ? ,description = ? ,use_flag = ? ,item_data_type = ? ,create_time = ? ,create_by = ? ,update_time = ? ,update_by = ? ,dict_type = ?  WHERE code = ?", entity.getName(), entity.getDescription(), entity.getUseFlag(), entity.getItemDataType(), entity.getCreateTime(), entity.getCreateBy(), entity.getUpdateTime(), entity.getUpdateBy(), entity.getDictType(), entity.getCode()) > 0;
    }

    @Override
    public boolean delete(SysDictType entity){
        return jdbcTemplate.update("DELETE FROM t_sys_dict_type WHERE code = ?", entity.getCode()) > 0;
    }

    @Override
    public SysDictType get(Serializable id) {
        List<SysDictType> list = jdbcTemplate.query("SELECT code, name, description, use_flag, item_data_type, create_time, create_by, update_time, update_by, dict_type FROM t_sys_dict_type WHERE code = ? ", new Object[]{ id }, new BeanPropertyRowMapper<SysDictType>(SysDictType.class));
        if(list != null && list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public List<SysDictType> findAll(Map< String, Object> params) {
        List<SysDictType> list = jdbcTemplate.query("SELECT code, name, description, use_flag, item_data_type, create_time, create_by, update_time, update_by, dict_type FROM t_sys_dict_type WHERE 1=1 ", new Object[]{}, new BeanPropertyRowMapper<SysDictType>(SysDictType.class));
        if(list != null && list.size() > 0) {
            return list;
        }else{
            return new ArrayList<>();
        }
    }

    @Override
    public boolean batchDelete(List ids) {
        String sql="DELETE FROM t_sys_dict_type WHERE code = ?";
        List< Object[] > batchArgs = new ArrayList<>();
        for(Object id : ids) {
            batchArgs.add(new Object[]{id});
        }
        return jdbcTemplate.batchUpdate(sql, batchArgs).length == ids.size();
    }

    @Override
    public Page<SysDictType> query(Page page, SysDictTypePo params) {
        if (page == null) {
            page = new Page();
        }
        List< Object > args = new ArrayList<>();
        StringBuilder where = new StringBuilder();
        if (!StringUtils.isEmpty(params.getSearchWord())) {
            where.append(" AND (code LIKE ? OR name LIKE ? OR description LIKE ?)");
            args.add("%" + params.getSearchWord() + "%");
            args.add("%" + params.getSearchWord() + "%");
            args.add("%" + params.getSearchWord() + "%");
        }
        if (!StringUtils.isEmpty(params.getDictType())) {
            where.append(" AND dict_type = ?");
            args.add(params.getDictType());
        }
        if (!StringUtils.isEmpty(params.getUseFlag())) {
            where.append(" AND use_flag = ?");
            args.add(params.getUseFlag());
        }
        Long total = jdbcTemplate.queryForObject(new StringBuffer("SELECT COUNT(1) FROM t_sys_dict_type WHERE 1=1 ").append(where).toString(), Long.class, args.toArray());
        if (total != null && total > 0) {
            args.add((page.getPageNo() - 1) * page.getLimit());
            args.add(page.getLimit());
            List<SysDictType> list = jdbcTemplate.query(new StringBuffer("SELECT code, name, description, use_flag, item_data_type, create_time, create_by, update_time, update_by, dict_type FROM t_sys_dict_type WHERE 1=1 ")
                    .append(where).append(" ORDER BY create_time DESC ").append(" LIMIT ?,?").toString(), args.toArray(), new BeanPropertyRowMapper<>(SysDictType.class));
            if(list != null && list.size() > 0) {
                page.setRecords(list);
            }
        }
        page.setTotal(total);
        return page;
    }
}
