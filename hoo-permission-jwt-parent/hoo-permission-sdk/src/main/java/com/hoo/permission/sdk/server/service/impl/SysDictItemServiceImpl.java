package com.hoo.permission.sdk.server.service.impl;

import com.hoo.common.model.Page;
import com.hoo.permission.sdk.server.dao.ISysDictItemDao;
import com.hoo.permission.sdk.server.domain.entity.SysDictItem;
import com.hoo.permission.sdk.server.domain.pojo.SysDictItemPo;
import com.hoo.permission.sdk.server.service.ISysDictItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 系统表 - 字典项 服务实现类
 * @author 小韩工作室
 * @date 2020-12-21 11:14:36
 */
@Service
public class SysDictItemServiceImpl implements ISysDictItemService {

    @Autowired ISysDictItemDao iDao;

    @Override
    public boolean save(SysDictItem entity) {
        return iDao.add(entity);
    }

    @Override
    public boolean delete(List ids) {
        return iDao.batchDelete(ids);
    }

    @Override
    public boolean update(SysDictItem entity) {
        return iDao.update(entity);
    }

    @Override
    public Page query(Page page, SysDictItemPo params) {
        if(page == null) {
            page = new Page();
        }
        iDao.query(page, params);
        return page;
    }
}

