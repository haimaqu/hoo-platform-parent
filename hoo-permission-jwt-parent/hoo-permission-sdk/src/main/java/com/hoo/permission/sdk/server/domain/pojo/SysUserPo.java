package com.hoo.permission.sdk.server.domain.pojo;

import com.hoo.permission.sdk.server.domain.entity.SysUser;

import java.util.List;

/**
 * 系统用户PO模型
 *
 * @author hank
 * @create 2020-06-29 下午5:23
 **/

public class SysUserPo extends SysUser {
    /**
     * 角色IDs
     */
    private List<Long> roleIds;
    /**
     * 弱角色场景，资源IDs
     */
    private List<Long> resourceIds;

    public List<Long> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<Long> roleIds) {
        this.roleIds = roleIds;
    }

    public List<Long> getResourceIds() {
        return resourceIds;
    }

    public void setResourceIds(List<Long> resourceIds) {
        this.resourceIds = resourceIds;
    }
}
