package com.hoo.permission.sdk.server.service.impl;

import com.hoo.common.model.Page;
import com.hoo.permission.sdk.server.dao.ISysAppDao;
import com.hoo.permission.sdk.server.domain.entity.SysApp;
import com.hoo.permission.sdk.server.domain.pojo.SysAppPo;
import com.hoo.permission.sdk.server.service.ISysAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 系统应用服务实现类
 *
 * @author hank
 * @create 2020-08-11 下午11:11
 **/
@Service
public class SysAppServiceImpl implements ISysAppService {

    @Autowired ISysAppDao sysAppDao;

    @Override
    public boolean save(SysApp entity) {
        return sysAppDao.add(entity);
    }

    @Override
    public boolean delete(List<Long> ids) {
        return sysAppDao.batchDelete(ids);
    }

    @Override
    public boolean update(SysApp entity) {
        SysApp temp = sysAppDao.get(entity.getId());
        if(temp == null) {
            return false;
        }
        // 单纯更新, secret不更新，单独方法用于重置 secret 即可
        entity.setSecret(temp.getSecret());
        return sysAppDao.update(entity);
    }

    @Override
    public Page query(Page page, SysAppPo params) {
        if(page == null) {
            page = new Page();
        }
        sysAppDao.query(page, params);
        return page;
    }
}
