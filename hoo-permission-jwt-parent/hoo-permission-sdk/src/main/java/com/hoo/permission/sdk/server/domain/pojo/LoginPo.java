package com.hoo.permission.sdk.server.domain.pojo;

/**
 * 登录PO模型
 *
 * @author hank
 * @create 2020-11-12 上午11:00
 **/

public class LoginPo {
    /**
     * 登录类型
     * @see {com.hoo.permission.sdk.server.domain.model.LoginType}
     */
    private String type;
    /**
     * 登录用户
     */
    private String username;
    /**
     * 登录密码
     */
    private String password;
    /**
     * 记住密码
     */
    private boolean rememberMe;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }
}
