package com.hoo.permission.sdk.server.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统表 - 字典项
 * @author 小韩工作室
 * @date 2020-12-21 09:37:51
 */
public class SysDictItem implements Serializable {
    /**
     * 自增组件
     */
    private Long id;
    /**
     * 所属字典类型编码
     */
    private String dictCode;
    /**
     * 字典项-显示值
     */
    private String label;
    /**
     * 字典项-实际值
     */
    private String value;
    /**
     * 用途描述说明
     */
    private String description;
    /**
     * 可选场景, 供前端灵活处理（逗号分隔, 如：该项可在查询是用，但是新增时禁选（不可见），修改可展示，但禁用）
     */
    private String optionalScenario;
    /**
     * 可用状态 1 可用， 0 不可用
     */
    private String useFlag;
    /**
     * 排序号
     */
    private Integer sortNum;
    /**
     * 创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**
     * 创建者
     */
    private String createBy;
    /**
     * 修改时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    /**
     * 修改者
     */
    private String updateBy;

    public Long getId(){
        return this.id;
    }

    public String getDictCode(){
        return this.dictCode;
    }

    public String getLabel(){
        return this.label;
    }

    public String getValue(){
        return this.value;
    }

    public String getDescription(){
        return this.description;
    }

    public String getOptionalScenario(){
        return this.optionalScenario;
    }

    public String getUseFlag(){
        return this.useFlag;
    }

    public Integer getSortNum(){
        return this.sortNum;
    }

    public Date getCreateTime(){
        return this.createTime;
    }

    public String getCreateBy(){
        return this.createBy;
    }

    public Date getUpdateTime(){
        return this.updateTime;
    }

    public String getUpdateBy(){
        return this.updateBy;
    }

    public void setId(Long id){
        this.id = id;
    }

    public void setDictCode(String dictCode){
        this.dictCode = dictCode;
    }

    public void setLabel(String label){
        this.label = label;
    }

    public void setValue(String value){
        this.value = value;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public void setOptionalScenario(String optionalScenario){
        this.optionalScenario = optionalScenario;
    }

    public void setUseFlag(String useFlag){
        this.useFlag = useFlag;
    }

    public void setSortNum(Integer sortNum){
        this.sortNum = sortNum;
    }

    public void setCreateTime(Date createTime){
        this.createTime = createTime;
    }

    public void setCreateBy(String createBy){
        this.createBy = createBy;
    }

    public void setUpdateTime(Date updateTime){
        this.updateTime = updateTime;
    }

    public void setUpdateBy(String updateBy){
        this.updateBy = updateBy;
    }

}
