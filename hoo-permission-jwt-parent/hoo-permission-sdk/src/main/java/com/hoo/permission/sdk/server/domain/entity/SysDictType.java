package com.hoo.permission.sdk.server.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统表 - 字典类型表
 * @author 小韩工作室
 * @date 2020-12-21 09:37:51
 */
public class SysDictType implements Serializable {
    /**
     * 字典类型编码【不可修改】
     */
    private String code;
    /**
     * 字典显示名称
     */
    private String name;
    /**
     * 用途描述说明
     */
    private String description;
    /**
     * 使用状态 1 可用 0 不可用
     */
    private String useFlag;
    /**
     * 元素数据类型: string 字符 , number 数字
     */
    private String itemDataType;
    /**
     * 数据字典类型: system 系统级，business 业务级
     */
    private String dictType;
    /**
     * 创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**
     * 创建者
     */
    private String createBy;
    /**
     * 修改时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    /**
     * 修改者
     */
    private String updateBy;

    public String getCode(){
        return this.code;
    }

    public String getName(){
        return this.name;
    }

    public String getDescription(){
        return this.description;
    }

    public String getUseFlag(){
        return this.useFlag;
    }

    public String getItemDataType(){
        return this.itemDataType;
    }

    public Date getCreateTime(){
        return this.createTime;
    }

    public String getCreateBy(){
        return this.createBy;
    }

    public Date getUpdateTime(){
        return this.updateTime;
    }

    public String getUpdateBy(){
        return this.updateBy;
    }

    public String getDictType(){
        return this.dictType;
    }

    public void setCode(String code){
        this.code = code;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public void setUseFlag(String useFlag){
        this.useFlag = useFlag;
    }

    public void setItemDataType(String itemDataType){
        this.itemDataType = itemDataType;
    }

    public void setCreateTime(Date createTime){
        this.createTime = createTime;
    }

    public void setCreateBy(String createBy){
        this.createBy = createBy;
    }

    public void setUpdateTime(Date updateTime){
        this.updateTime = updateTime;
    }

    public void setUpdateBy(String updateBy){
        this.updateBy = updateBy;
    }

    public void setDictType(String dictType){
        this.dictType = dictType;
    }

}
