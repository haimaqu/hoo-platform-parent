package com.hoo.permission.sdk.web.api.system;

import com.hoo.common.model.Page;
import com.hoo.common.model.R;
import com.hoo.permission.sdk.server.domain.entity.SysDictItem;
import com.hoo.permission.sdk.server.domain.entity.SysDictType;
import com.hoo.permission.sdk.server.domain.pojo.SysDictItemPo;
import com.hoo.permission.sdk.server.domain.pojo.SysDictTypePo;
import com.hoo.permission.sdk.server.service.ISysDictItemService;
import com.hoo.permission.sdk.server.service.ISysDictTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 字典表相关接口实现
 *
 * @author hank
 * @create 2020-12-21 下午1:54
 **/
@RestController
@RequestMapping("dict")
public class DictController {

    @Autowired ISysDictTypeService typeService;
    @Autowired ISysDictItemService itemService;

    @PostMapping("type")
    public R addType(SysDictType entity){
        return R.data(typeService.save(entity));
    }

    @PostMapping("item")
    public R addItem(SysDictItem entity){
        return R.data(itemService.save(entity));
    }

    @PutMapping("type")
    public R updateType(SysDictType entity){
        return R.data(typeService.update(entity));
    }

    @PutMapping("item")
    public R updateItem(SysDictItem entity){
        return R.data(itemService.update(entity));
    }

    @DeleteMapping("type/{ids}")
    public R delType(@PathVariable("ids") String ids) {
        List<String> codes = new ArrayList<>();
        for(String id : ids.split(",")) {
            codes.add(id);
        }
        return R.data(typeService.delete(codes));
    }

    @DeleteMapping("item/{ids}")
    public R delItem(@PathVariable("ids") String ids){
        List<Long> list = new ArrayList<>();
        for(String id : ids.split(",")) {
            list.add(Long.valueOf(id));
        }
        return R.data(itemService.delete(list));
    }

    @GetMapping("type")
    public R queryTypes(Page page, SysDictTypePo params){
        return R.page(typeService.query(page, params));
    }

    @GetMapping("type/dict")
    public R dicTypes(SysDictTypePo params) {
        ArrayList<Map<String,Object>> list = new ArrayList<Map<String, Object>>();
        Page page = new Page(1, 10000);
        page = typeService.query(page, params);
        for (int i=0,len = page.getRecords().size(); i<len; i++) {
            SysDictType record = (SysDictType)page.getRecords().get(i);
            Map<String,Object> map = new HashMap<String,Object>();
            map.put("label", record.getName());
            map.put("value", record.getCode());
            list.add(map);
        }
        return R.data(list);
    }

    @GetMapping("item")
    public R queryItems(Page page, SysDictItemPo params){
        return R.page(itemService.query(page, params));
    }

    @GetMapping("item/all")
    public R queryItemAll () {
        // 后续考虑缓存 及优化 -- 需考虑到 dict type !!
        ArrayList<Map<String,Object>> list = new ArrayList<Map<String, Object>>();
        SysDictItemPo params = new SysDictItemPo();
        params.setUseFlag("1");
        Page page = new Page(1, 300000);
        page = itemService.query(page, params);
        for (int i=0,len = page.getRecords().size(); i<len; i++) {
            SysDictItem record = (SysDictItem)page.getRecords().get(i);
            Map<String,Object> map = new HashMap<String,Object>();
            map.put("type", record.getDictCode());
            map.put("label", record.getLabel());
            map.put("value", record.getValue());
            list.add(map);
        }
        return R.data(list);
    }

}
