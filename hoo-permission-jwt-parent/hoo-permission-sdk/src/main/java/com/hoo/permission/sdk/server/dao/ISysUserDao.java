package com.hoo.permission.sdk.server.dao;

import com.hoo.common.model.Page;
import com.hoo.permission.sdk.server.domain.dto.SysUserDto;
import com.hoo.permission.sdk.server.domain.entity.SysUser;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 系统-用户表 Dao层接口
 * @author 小韩工作室
 * @date 2020-06-28 16:51:32
 */
public interface ISysUserDao {
    /**
     * 新增 - 系统-用户表
     */
    boolean add(SysUser entity);

    /**
     * 修改 - 系统-用户表
     */
    boolean update(SysUser entity);

    /**
     * 仅变更用户状态
     * @param id
     * @param status
     * @return
     */
    boolean updateStatus(Long id, int status);

    /**
     * 删除 - 系统-用户表
     */
    boolean delete(SysUser entity);

    /**
     * 根据主键 获取记录
     */
    SysUser get(Serializable id);

    /**
     * 根据 username 获取用户信息
     * @param username
     * @return
     */
    SysUser getByUsername(String username);

    /**
     * 根据条件查询所有记录
     * @param params
     * @return
     */
    List<SysUserDto> findAll(Map<String, Object> params);

    /**
     * 用户查询 - 分页
     * @param page
     * @param params
     * @return
     */
    List<SysUserDto> query(Page page, Map<String, Object> params);

    /**
     * 根据用户ID 获取权限信息
     * @param userId
     * @return
     */
    List<String> queryAllPerms(Long userId);
}
