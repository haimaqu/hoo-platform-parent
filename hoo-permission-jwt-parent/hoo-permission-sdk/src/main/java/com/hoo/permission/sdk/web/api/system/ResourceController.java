package com.hoo.permission.sdk.web.api.system;

import com.hoo.common.model.R;
import com.hoo.permission.sdk.server.domain.entity.SysResource;
import com.hoo.permission.sdk.server.domain.model.ResourceStatus;
import com.hoo.permission.sdk.server.service.ISysResourceService;
import com.hoo.permission.sdk.server.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 资源控制器
 * 1、资源分页查询（列表分页展现-平铺，不常用）
 * 2、资源排序展示（排序处理后，前端树形）
 * 3、资源菜单树(可根据parentId)
 * 4、新增资源（菜单、按钮）、修改 & 删除 - 按钮不允许包含子节点
 * @author hank
 * @create 2020-07-04 上午11:00
 **/

@Api(tags = "资源管理", description = "菜单、按钮的CRUD功能")
@RestController
@RequestMapping("resource")
public class ResourceController {

    @Autowired ISysResourceService resourceService;

    /*@GetMapping
    public R query(Page page, Map<String,Object> params) {
        return R.page(resourceDao.query(page, params));
    }*/

    @ApiOperation("资源信息转字典格式")
    @GetMapping("/dictionary")
    public R dictionary(Long parentId, Integer type, String status) {
        Map<String,Object> params = new HashMap<>();
        params.put("parentId", parentId);
        params.put("type", type);
        params.put("status", status);
        List<SysResource> list = resourceService.queryList(params);
        List<Map<String,Object>> res = new ArrayList<>();
        list.stream().forEach(record -> {
            Map<String,Object> item = new HashMap<>();
            item.put("value", record.getId());
            item.put("label", record.getName());
            item.put("category", record.getType());
            item.put("id", record.getId());
            item.put("parentId", record.getParentId());
            res.add(item);
        });
        return R.data(res);
    }

    @ApiOperation("资源列表集合")
    @GetMapping("/list")
    @RequiresPermissions("sys:resource:list")
    public R query(Long parentId, Integer type, String searchWord, String status) {
        Map<String,Object> params = new HashMap<>();
        params.put("parentId", parentId);
        params.put("type", type);
        params.put("status", status);
        params.put("searchWord", searchWord);
        return R.data(resourceService.queryList(params));
    }

    /**
     * 获取树形资源（菜单 + 按钮， 通过 type 可过滤）
     * @param parentId
     * @param type
     * @param searchWord
     * @param status
     * @return
     */
    @ApiOperation("资源树形格式集合")
    @GetMapping("/tree")
    @RequiresPermissions("sys:resource:list")
    public R queryTree(Long parentId, Integer type, String searchWord, String status) {
        Map<String,Object> params = new HashMap<>();
        params.put("parentId", parentId);
        params.put("type", type);
        params.put("searchWord", searchWord);
        params.put("status", status);
        return R.data(resourceService.queryTree(params));
    }


    /**
     * 新增资源
     * @param resource
     */
    @ApiOperation("新增资源")
    @PostMapping
    @RequiresPermissions("sys:resource:save")
    public R save(SysResource resource) {
        resourceService.add(resource);
        return R.ok();
    }

    /**
     * 修改资源
     * @param resource
     * @return
     */
    @ApiOperation("修改资源")
    @PutMapping
    @RequiresPermissions("sys:resource:update")
    public R update(SysResource resource) {
        resourceService.update(resource);
        return R.ok();
    }

    @ApiOperation("修改资源状态")
    @PutMapping("/updateStatus")
    @RequiresPermissions({"sys:resource:disabled", "sys:resource:useable"})
    public R updateStatus(Long id, String status) {
        if (!ResourceStatus.DISABLED.equals(status)) {
            status = ResourceStatus.NORMAL;
        }
        SysResource entity = resourceService.get(id);
        if (entity == null) {
            return R.error("记录不存在或已删除");
        }
        entity.setStatus(status);
        return R.data(resourceService.update(entity));
    }

    /**
     * （批量）删除资源
     * @param ids
     * @return
     */
    @ApiOperation("删除资源")
    @DeleteMapping("/{ids}")
    @RequiresPermissions("sys:resource:delete")
    public R delete(@PathVariable("ids") String ids) {
        resourceService.delete(StringUtil.parseLong(ids));
        return R.ok();
    }

}
