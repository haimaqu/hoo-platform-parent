package com.hoo.permission.sdk.server.dao;

import com.hoo.common.model.Page;
import com.hoo.permission.sdk.server.domain.entity.SysDictItem;
import com.hoo.permission.sdk.server.domain.pojo.SysDictItemPo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 系统表 - 字典项 Dao层接口
 * @author 小韩工作室
 * @date 2020-12-21 09:37:51
 */
public interface ISysDictItemDao {
    /**
     * 新增 - 系统表 - 字典项
     */
    boolean add(SysDictItem entity);

    /**
     * 修改 - 系统表 - 字典项
     */
    boolean update(SysDictItem entity);

    /**
     * 删除 - 系统表 - 字典项
     * @param entity
     * @return
     */
    boolean delete(SysDictItem entity);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    boolean batchDelete(List ids);

    /**
     * 批量删除
     * @param codes 所属编码
     * @return
     */
    boolean batchDeleteByCodes(List codes);

    /**
     * 根据主键 获取记录
     */
    SysDictItem get(Serializable id);

    /**
     * 根据条件查询所有记录
     */
    List<SysDictItem> findAll(Map< String, Object> params);

    /**
     * 分页查询处理
     * @param page
     * @param params
     * @return
     */
    Page<SysDictItem> query(Page page, SysDictItemPo params);
}
