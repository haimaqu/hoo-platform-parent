package com.hoo.permission.sdk.server.service;

import com.hoo.common.model.Page;
import com.hoo.permission.sdk.server.domain.entity.SysDictType;
import com.hoo.permission.sdk.server.domain.pojo.SysDictTypePo;

import java.util.List;
import java.util.Map;

/**
 * 系统表 - 字典类型表 服务接口
 * @author 小韩工作室
 * @date 2020-12-21 11:14:36
 */
public interface ISysDictTypeService {

    /**
    * 新增
    * @param entity
    * @return
    */
    boolean save(SysDictType entity);

    /**
    * 删除
    * @param ids
    * @return
    */
    boolean delete(List ids);

    /**
    * 修改
    * @param entity
    * @return
    */
    boolean update(SysDictType entity);

    /**
    * 分页查询
    * @param page
    * @param params
    * @return
    */
    Page query(Page page, SysDictTypePo params);

}
