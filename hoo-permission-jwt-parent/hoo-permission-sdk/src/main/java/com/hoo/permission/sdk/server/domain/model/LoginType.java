package com.hoo.permission.sdk.server.domain.model;

/**
 * 登录方式
 * @author hank
 */
public interface LoginType {
    /**
     * 简单登录：用户名 + 密码
     */
    String SIMPLE = "simple";
}
