package com.hoo.permission.sdk.server.dao.impl;

import com.hoo.common.model.Page;
import com.hoo.permission.sdk.server.dao.ISysDictItemDao;
import com.hoo.permission.sdk.server.domain.entity.SysDictItem;
import com.hoo.permission.sdk.server.domain.pojo.SysDictItemPo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 系统表 - 字典项 Dao层 实现
 * @author 小韩工作室
 * @date 2020-12-21 09:49:31
 */
@Repository
public class SysDictItemDaoImpl implements ISysDictItemDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public boolean add(SysDictItem entity){
        entity.setCreateTime(new Date());
        return jdbcTemplate.update("INSERT INTO t_sys_dict_item(id, dict_code, label, value, description, optional_scenario, use_flag, sort_num, create_time, create_by, update_time, update_by) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", entity.getId(), entity.getDictCode(), entity.getLabel(), entity.getValue(), entity.getDescription(), entity.getOptionalScenario(), entity.getUseFlag(), entity.getSortNum(), entity.getCreateTime(), entity.getCreateBy(), entity.getUpdateTime(), entity.getUpdateBy()) > 0;
    }

    @Override
    public boolean update(SysDictItem entity) {
        entity.setUpdateTime(new Date());
        return jdbcTemplate.update("UPDATE t_sys_dict_item SET id = ? ,dict_code = ? ,label = ? ,value = ? ,description = ? ,optional_scenario = ? ,use_flag = ? ,sort_num = ? ,create_time = ? ,create_by = ? ,update_time = ? ,update_by = ?  WHERE  id = ?", entity.getId(), entity.getDictCode(), entity.getLabel(), entity.getValue(), entity.getDescription(), entity.getOptionalScenario(), entity.getUseFlag(), entity.getSortNum(), entity.getCreateTime(), entity.getCreateBy(), entity.getUpdateTime(), entity.getUpdateBy(), entity.getId()) > 0;
    }

    @Override
    public boolean delete(SysDictItem entity){
        return jdbcTemplate.update("DELETE FROM t_sys_dict_item WHERE id = ?", entity.getId()) > 0;
    }

    @Override
    public SysDictItem get(Serializable id) {
        List<SysDictItem> list = jdbcTemplate.query("SELECT id, dict_code, label, value, description, optional_scenario, use_flag, sort_num, create_time, create_by, update_time, update_by FROM t_sys_dict_item WHERE  = ? ", new Object[]{ id }, new BeanPropertyRowMapper<SysDictItem>(SysDictItem.class));
        if(list != null && list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public List<SysDictItem> findAll(Map< String, Object> params) {
        List<SysDictItem> list = jdbcTemplate.query("SELECT id, dict_code, label, value, description, optional_scenario, use_flag, sort_num, create_time, create_by, update_time, update_by FROM t_sys_dict_item WHERE 1=1 ", new Object[]{}, new BeanPropertyRowMapper<SysDictItem>(SysDictItem.class));
        if(list != null && list.size() > 0) {
            return list;
        }else{
            return new ArrayList<>();
        }
    }

    @Override
    public boolean batchDelete(List ids) {
        String sql="DELETE FROM t_sys_dict_item WHERE id = ?";
        List< Object[] > batchArgs = new ArrayList<>();
        for(Object id : ids) {
            batchArgs.add(new Object[]{id});
        }
        return jdbcTemplate.batchUpdate(sql, batchArgs).length == ids.size();
    }

    @Override
    public boolean batchDeleteByCodes(List codes) {
        String sql="DELETE FROM t_sys_dict_item WHERE dict_code = ?";
        List< Object[] > batchArgs = new ArrayList<>();
        for(Object id : codes) {
            batchArgs.add(new Object[]{id});
        }
        return jdbcTemplate.batchUpdate(sql, batchArgs).length == codes.size();
    }

    @Override
    public Page<SysDictItem> query(Page page, SysDictItemPo params) {
        if (page == null) {
            page = new Page();
        }
        List<Object> args = new ArrayList<>();
        StringBuilder where = new StringBuilder();
        if (!StringUtils.isEmpty(params.getSearchWord())) {
            where.append(" AND (label LIKE ? OR value LIKE ? OR description LIKE ?)");
            args.add("%" + params.getSearchWord() + "%");
            args.add("%" + params.getSearchWord() + "%");
            args.add("%" + params.getSearchWord() + "%");
        }
        if (!StringUtils.isEmpty(params.getDictCode())) {
            where.append(" AND dict_code = ?");
            args.add(params.getDictCode());
        }
        if (!StringUtils.isEmpty(params.getDictCodes()) && params.getDictCodes().split(",").length > 0) {
            String[] dictCodes = params.getDictCodes().split(",");
            where.append(" AND (");
            for(int i = 0,len = dictCodes.length; i < len; i++){
                String dictCode = dictCodes[i];
                where.append("dict_code = ? ").append(i == len - 1 ? "" : " OR ");
                args.add(dictCode);
            }
            where.append(" )");
        }
        if(!StringUtils.isEmpty(params.getOptionalScenario())) {
            where.append(" AND optional_scenario = ?");
            args.add(params.getOptionalScenario());
        }
        if(!StringUtils.isEmpty(params.getUseFlag())) {
            where.append(" AND use_flag = ?");
            args.add(params.getUseFlag());
        }
        // 查询参数 处理
        Long total = jdbcTemplate.queryForObject(new StringBuffer("SELECT COUNT(1) FROM t_sys_dict_item WHERE 1=1 ").append(where).toString(), Long.class, args.toArray());
        if (total != null && total > 0) {
            args.add((page.getPageNo() - 1) * page.getLimit());
            args.add(page.getLimit());
            List<SysDictItem> list = jdbcTemplate.query(new StringBuffer("SELECT id, dict_code, label, value, description, optional_scenario, use_flag, sort_num, create_time, create_by, update_time, update_by FROM t_sys_dict_item WHERE 1=1 ")
                    .append(where).append(" ORDER BY create_time DESC,dict_code ASC, sort_num ASC").append(" LIMIT ?,?").toString(), args.toArray(), new BeanPropertyRowMapper<>(SysDictItem.class));
            if(list != null && list.size() > 0) {
                page.setRecords(list);
            }
        }
        page.setTotal(total);
        return page;
    }

    // 字典类型 type; 字典值 value; 字典显示值 label; 可选场景


}
