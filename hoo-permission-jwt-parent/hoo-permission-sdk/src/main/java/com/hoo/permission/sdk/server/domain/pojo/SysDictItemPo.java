package com.hoo.permission.sdk.server.domain.pojo;

import com.hoo.permission.sdk.server.domain.entity.SysDictItem;

/**
 * 系统表 - 字典项
 *
 * @author hank
 * @create 2020-12-21 上午9:40
 **/

public class SysDictItemPo extends SysDictItem {
    /**
     * 搜索关键词 = 显示名称 + 值 +  描述
     */
    private String searchWord;
    /**
     * 过滤条件：所属字典code 集合 逗号分隔
     */
    private String dictCodes;

    public String getSearchWord() {
        return searchWord;
    }

    public void setSearchWord(String searchWord) {
        this.searchWord = searchWord;
    }

    public String getDictCodes() {
        return dictCodes;
    }

    public void setDictCodes(String dictCodes) {
        this.dictCodes = dictCodes;
    }

}
