package com.hoo.permission.sdk.server.dao;

import com.hoo.common.model.Page;
import com.hoo.permission.sdk.server.domain.entity.SysDictType;
import com.hoo.permission.sdk.server.domain.pojo.SysDictTypePo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 系统表 - 字典类型表 Dao层接口
 * @author 小韩工作室
 * @date 2020-12-21 09:37:51
 */
public interface ISysDictTypeDao {
    /**
     * 新增 - 系统表 - 字典类型表
     */
    boolean add(SysDictType entity);

    /**
     * 修改 - 系统表 - 字典类型表
     */
    boolean update(SysDictType entity);

    /**
     * 删除 - 系统表 - 字典类型表
     */
    boolean delete(SysDictType entity);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    boolean batchDelete(List ids);

    /**
     * 根据主键 获取记录
     */
    SysDictType get(Serializable id);

    /**
     * 根据条件查询所有记录
     */
    List<SysDictType> findAll(Map< String, Object> params);

    /**
     * 分页查询处理
     * @param page
     * @param params
     * @return
     */
    Page<SysDictType> query(Page page, SysDictTypePo params);
}
