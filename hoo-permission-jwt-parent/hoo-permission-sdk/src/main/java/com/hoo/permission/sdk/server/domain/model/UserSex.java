package com.hoo.permission.sdk.server.domain.model;
/**
 * 用户性别
 * com.hoo.permission.sdk.server.domain.entity.SysUser.sex
 * @author hank
 * @create 2020-07-01 下午10:49
 **/

public interface UserSex {
    /**
     * 未知/保密
     */
    String UNKNOW = "UNKNOW";
    /**
     * 男性
     */
    String MALE = "MALE";
    /**
     * 女性
     */
    String FEMALE = "FEMALE";
}
