package com.hoo.permission.sdk.web.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

/**
 * 安全配置
 *
 * @author hank
 * @create 2020-07-01 下午11:15
 **/
@Configuration
@ConfigurationProperties(prefix = "hoo.permission.safe")
public class SafeConfig {
    /**
     * hash 加密次数，，多套系统在复用同一套库时，需保证一致「此时建议走统一认证中心」
     */
    private Integer hashIterations;
    /**
     * hash 加密方式
     */
    private String hashAlgorithmName;
    /**
     * 用户默认密码
     */
    private String userPassword;

    public Integer getHashIterations() {
        return hashIterations == null ? 3 : hashIterations;
    }

    public void setHashIterations(Integer hashIterations) {
        this.hashIterations = hashIterations;
    }

    public String getHashAlgorithmName() {
        return StringUtils.isEmpty(hashAlgorithmName) ? "MD5" : hashAlgorithmName;
    }

    public void setHashAlgorithmName(String hashAlgorithmName) {
        this.hashAlgorithmName = hashAlgorithmName;
    }

    public String getUserPassword() {
        return userPassword == null ? "123456" : userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
}
