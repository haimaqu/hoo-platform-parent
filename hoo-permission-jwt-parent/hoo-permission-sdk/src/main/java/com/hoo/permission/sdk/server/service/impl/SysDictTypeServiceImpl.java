package com.hoo.permission.sdk.server.service.impl;

import com.hoo.common.model.Page;
import com.hoo.permission.sdk.server.dao.ISysDictItemDao;
import com.hoo.permission.sdk.server.dao.ISysDictTypeDao;
import com.hoo.permission.sdk.server.domain.entity.SysDictType;
import com.hoo.permission.sdk.server.domain.pojo.SysDictTypePo;
import com.hoo.permission.sdk.server.service.ISysDictTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 系统表 - 字典类型表 服务实现类
 * @author 小韩工作室
 * @date 2020-12-21 11:14:36
 */
@Service
public class SysDictTypeServiceImpl implements ISysDictTypeService {

    @Autowired ISysDictTypeDao iDao;

    @Autowired ISysDictItemDao itemDao;

    @Override
    public boolean save(SysDictType entity) {
        entity.setCreateTime(new Date());
        return iDao.add(entity);
    }

    @Override
    public boolean delete(List ids) {
        return iDao.batchDelete(ids) && itemDao.batchDeleteByCodes(ids);
    }

    @Override
    public boolean update(SysDictType entity) {
        entity.setUpdateTime(new Date());
        return iDao.update(entity);
    }

    @Override
    public Page query(Page page, SysDictTypePo params) {
        if(page == null) {
            page = new Page();
        }
        iDao.query(page, params);
        return page;
    }
}

