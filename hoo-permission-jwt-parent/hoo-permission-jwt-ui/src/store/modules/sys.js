import { getResource, getRoles } from '@/api/sys'

const user = {
  state: {
    resource: [],
    allRoles: []
  },

  mutations: {
    SET_RESOURCE: (state, resource) => {
      state.resource = resource
    },
    SET_ALL_ROLES: (state, roles) => {
      state.allRoles = roles
    }
  },

  actions: {
    AllResource ({ commit }) {
      return new Promise((resolve, reject) => {
        getResource({ status: '1' }).then(res => {
          commit('SET_RESOURCE', res.data || [])
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },
    AllRoles ({ commit }) {
      return new Promise((resolve, reject) => {
        getRoles({ status: '1' }).then(res => {
          commit('SET_ALL_ROLES', res.data || [])
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default user
