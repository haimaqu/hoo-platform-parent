// import api from './index'
import { axios } from '@/utils/request'

export function getResource (parameter) {
  return axios({
    url: '/resource/dictionary',
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

export function getRoles (parameter) {
  return axios({
    url: '/role/dictionary',
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

export function getDict (parameter) {
  return axios({
    url: '/dict/item/all',
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
