import Vue from 'vue'
import moment from 'moment'
import 'moment/locale/zh-cn'
import dic from './dic'
moment.locale('zh-cn')

Vue.filter('NumberFormat', function (value) {
  if (!value) {
    return '0'
  }
  const intPartFormat = value.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,') // 将整数部分逢三一断
  return intPartFormat
})

Vue.filter('dayjs', function (dataStr, pattern = 'YYYY-MM-DD HH:mm:ss') {
  return moment(dataStr).format(pattern)
})

Vue.filter('moment', function (dataStr, pattern = 'YYYY-MM-DD HH:mm:ss') {
  return moment(dataStr).format(pattern)
})

Vue.filter('getLabel', function (value, type) {
  const valLabels = dic.query(type)
  if (typeof value === 'string' && value.indexOf('、')) {
    value = value.split('、')
  }
  if (value && value instanceof Array) {
    const labels = []
    valLabels.some(item => {
      if (value.indexOf(item.value) > -1) {
        labels.push(item.label)
      }
      if (labels.length === value.length) {
        return true
      }
    })
    return labels.join('、')
  }
  let label = typeof value === 'undefined' ? null : (value)
  valLabels.some(item => {
    if (item.value === label) {
      label = item.label
      return true
    }
  })
  return label
})
